/* $Header: $ */

/* Patrick J. Thomas, California Institute of Technology, LIGO Hanford */


#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include <QObject>

#include <QPixmap>

class AcquisitionType;

struct gstreamer_data {
	GstElement* pipeline;
	GstElement* udp_source;
	GstElement* rtp_decoder;
	GstElement* video_decoder;
	GstElement* video_converter;
	GstElement* app_sink;
	gulong signal_handler_id;
	AcquisitionType* instance;
};


class AcquisitionType : public QObject
{
	Q_OBJECT

public:
	AcquisitionType(char const* address, gint port, guint64 udp_timeout);
	~AcquisitionType();

signals:
	void setData(const QPixmap&);

private:
	static GstFlowReturn new_sample_callback(GstAppSink* appsink, gstreamer_data* user_data);
	static GstPadProbeReturn udp_source_buffer_pad_probe_callback(GstPad* pad, GstPadProbeInfo* info, gstreamer_data* user_data);
	static void udp_source_timeout_callback(GstBus* bus, GstMessage* message, gstreamer_data* user_data);
	static void bus_error_callback(GstBus* bus, GstMessage* message, gstreamer_data* user_data);

	gstreamer_data data;
};

